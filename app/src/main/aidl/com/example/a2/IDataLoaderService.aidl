package com.example.a2;

import com.example.a2.IDataLoaderServiceListener;

interface IDataLoaderService {
    void fetchData(IDataLoaderServiceListener listener);
}