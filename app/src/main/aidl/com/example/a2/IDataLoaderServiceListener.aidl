package com.example.a2;

interface IDataLoaderServiceListener {
    void dataLoaded(String data);
}
