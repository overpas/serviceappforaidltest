package com.example.a2;

import android.app.Application;

/**
 * Created by Pavel Shurmilov on 14.04.2018.
 */

public class App extends Application {

    private static volatile App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized App getInstance() {
        return instance;
    }

}
