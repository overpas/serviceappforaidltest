package com.example.a2;

/**
 * Created by Pavel Shurmilov on 14.04.2018.
 */

public interface Constants {
    int CONNECTION_TIMEOUT_MILLISECONDS = 3000;
    String HTTP_VERB = "GET";
    String CHARSET_NAME = "UTF-8";
    String HUMAN_DATA_FETCHING_URL = "http://demo7326120.mockable.io/";
}
