package com.example.a2;

import android.util.Log;

/**
 * Created by Pavel Shurmilov on 14.04.2018.
 */

public class Logger {

    public static void error(String tag, String msg, Throwable e) {
        Log.e(tag, msg, e);
    }

    public static void error(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static void info(String tag, String msg, Throwable e) {
        Log.i(tag, msg, e);
    }

    public static void info(String tag, String msg) {
        Log.i(tag, msg);
    }

}
