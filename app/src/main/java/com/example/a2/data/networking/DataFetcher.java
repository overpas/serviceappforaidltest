package com.example.a2.data.networking;

import com.example.a2.App;
import com.example.a2.Constants;
import com.example.a2.R;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Pavel Shurmilov on 27.03.2018.
 */

public class DataFetcher {

    public static String getData(String serverUrl) throws IOException {
        URL url = new URL(serverUrl);
        InputStream stream;
        HttpURLConnection connection;
        String result = null;
        connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(Constants.CONNECTION_TIMEOUT_MILLISECONDS);
        connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT_MILLISECONDS);
        connection.setRequestMethod(Constants.HTTP_VERB);
        connection.setDoInput(true);
        connection.connect();
        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new IOException(App.getInstance().getString(R.string.http_error_prefix) + responseCode);
        }
        stream = connection.getInputStream();
        if (stream != null) {
            result = readStream(stream);
            stream.close();
        }
        connection.disconnect();
        return result;
    }

    private static String readStream(InputStream stream) throws IOException {
        Reader reader = new InputStreamReader(stream, Constants.CHARSET_NAME);
        StringBuffer buffer = new StringBuffer();
        int data;
        while ((data = reader.read()) != -1) {
            buffer.append((char) data);
        }
        reader.close();
        return buffer.toString();
    }

}
