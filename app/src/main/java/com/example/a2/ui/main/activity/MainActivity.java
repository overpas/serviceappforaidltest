package com.example.a2.ui.main.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;

import com.example.a2.IDataLoaderService;
import com.example.a2.R;
import com.example.a2.ui.main.service.RemotelyUsedLoaderService;

public class MainActivity extends AppCompatActivity implements ServiceConnection {

    private IDataLoaderService loaderService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, RemotelyUsedLoaderService.class);
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(this, RemotelyUsedLoaderService.class);
        stopService(intent);
        super.onDestroy();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        loaderService = IDataLoaderService.Stub.asInterface(iBinder);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        loaderService = null;
    }

}
