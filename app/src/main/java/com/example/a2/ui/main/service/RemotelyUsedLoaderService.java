package com.example.a2.ui.main.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;

import com.example.a2.Constants;
import com.example.a2.IDataLoaderService;
import com.example.a2.IDataLoaderServiceListener;
import com.example.a2.Logger;
import com.example.a2.R;
import com.example.a2.data.networking.DataFetcher;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RemotelyUsedLoaderService extends Service {

    private static final String TAG = RemotelyUsedLoaderService.class.getSimpleName();
    private ExecutorService executorService;
    private Handler thisHandler = new Handler();

    private IDataLoaderService.Stub serviceBinder = new IDataLoaderService.Stub() {
        @Override
        public void fetchData(final IDataLoaderServiceListener listener) throws RemoteException {
            Logger.info(TAG, getString(R.string.data_request_received_message));
            getDataFor(listener);
        }
    };

    public RemotelyUsedLoaderService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.info(TAG, getString(R.string.service_started_message));
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public IBinder onBind(Intent intent) {
        Logger.info(TAG, getString(R.string.on_bind_called_message));
        return serviceBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Logger.info(TAG, getString(R.string.on_rebind_called_message));
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Logger.info(TAG, getString(R.string.on_unbind_called_message));
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Logger.info(TAG, getString(R.string.service_destroyed_message));
        executorService.shutdownNow();
        super.onDestroy();
    }

    private synchronized void getDataFor(final IDataLoaderServiceListener listener) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                String data = null;
                try {
                    data = DataFetcher.getData(Constants.HUMAN_DATA_FETCHING_URL);
                } catch (IOException e) {
                    Logger.error(TAG, e.getMessage(), e);
                    notifyListener(null, listener);
                }
                if (Thread.currentThread().isInterrupted()) {
                    notifyListener(null, listener);
                } else {
                    notifyListener(data, listener);
                }
            }
        });
    }

    private synchronized void notifyListener(final String resultData, final IDataLoaderServiceListener listener) {
        thisHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    listener.dataLoaded(resultData);
                } catch (RemoteException e) {
                    Logger.error(TAG, e.getMessage(), e);
                }
            }
        });
    }

}
